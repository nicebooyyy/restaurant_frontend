import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'http://localhost:3000/api/test/';

class UserService {
  getPublicContent() {
    return axios.get(API_URL + 'all');
  }

  getUserBoard() {
    return axios.get(API_URL + 'user', { headers: authHeader() });
  }

  getModeratorBoard() {
    return axios.get(API_URL + 'mod', { headers: authHeader() });
  }

  getCookBoard() {
    return axios.get(API_URL + 'cook', { headers: authHeader() });
  }

  getWaiterBoard() {
    return axios.get(API_URL + 'waiter', { headers: authHeader() });
  }

  // getForecastBoard() {
  //   return axios.get('http://localhost:3000/holt', { headers: authHeader() });
  // }

  getAdminBoard() {
    return axios.get(API_URL + 'admin', { headers: authHeader() });
  }
}

export default new UserService();