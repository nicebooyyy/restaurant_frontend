import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from "vuex-persistedstate";

import { auth } from './auth.module';
import dishes from './modules/dishes';
import waiter from './modules/waiter';

Vue.use(Vuex)

export default new Vuex.Store({
  //визачаємо початкові дані
  state: {
    
  },
  //функції які змінюють store(сховище)
  mutations: {
  },
  actions: {
  },
  //трансформація даних та отримання їх зі сховища
  getters: {

  },
  modules: {
    auth,
    dishes,
    waiter
  },
  //плагін для збереження даних в куках
  plugins: [createPersistedState()],
})
