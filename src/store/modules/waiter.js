import axios from 'axios';


export default {
    //визачаємо початкові дані(початковий стан)
    state: {
        cartItemCountWaiter: 0,
        cartItemsWaiter: [],
        dishesWaiter: [],
    },

    //функції які змінюють store(сховище)
    mutations: {
        //заношу список страв в сховище
        updateStateDishesWaiter (state,dishesWaiter) {
            state.dishesWaiter = dishesWaiter;
        },
        addToCartWaiter (state,payload) {
            let item = payload;
            
            item = {...item, quantity: 1};
            // console.log(item);
            if (state.cartItemsWaiter.length >= 1) {
               let bool = state.cartItemsWaiter.some(i=>i._id === item._id);
               if (bool) {
                   let itemIndex = state.cartItemsWaiter.findIndex(el=>el._id === item._id);
                   state.cartItemsWaiter[itemIndex]["quantity"] += 1;
               } else {
                   state.cartItemsWaiter.push(item);
               }
            } else {
                
                state.cartItemsWaiter.push(item);
            }
            state.cartItemCountWaiter++;
        },
        removeItemWaiter (state,payload) {
            if (state.cartItemsWaiter.length > 0) {
                let bool = state.cartItemsWaiter.some(i=>i._id === payload._id)
                if (bool) {
                    let index = state.cartItemsWaiter.findIndex(el=>el._id === payload._id)
                   if (state.cartItemsWaiter[index]["quantity"] !== 0) {
                        state.cartItemsWaiter[index]["quantity"] -= 1
                        state.cartItemCountWaiter--
                   }
                   if (state.cartItemsWaiter[index]["quantity"] === 0) {
                    state.cartItemsWaiter.splice(index,1)
                    }
                    
                }
            }
        },
        clearCartWaiter (state) {
            state.cartItemsWaiter = [];
            state.cartItemCountWaiter= 0;
            
		},
    },
    
	
    //для роботи з бекендом
    actions: {
        //отримую список страв з сервера
        async serverDishesWaiter (context) {
            const response  = await axios.get('http://localhost:3000/dishes');
            const dishesWaiter =  response.data.dishesWaiter;
            context.commit('updateStateDishesWaiter',dishesWaiter);
        },
       addToCartWaiter: (context,payload) => {
           context.commit("addToCartWaiter", payload);
       },
       removeItemWaiter: (context,payload) => {
        context.commit("removeItemWaiter", payload);
        },
        clearCartWaiter: (context) => {
            context.commit('clearCartWaiter');
        }
    },

    //трансформація даних та отримання їх зі сховища
    getters: {
        allDishesWaiter (state) {
            console.log(state.dishesWaiter);
            return state.dishesWaiter;
        },
        cartItemCountWaiter (state) {
            return state.cartItemCountWaiter;
        },
    },
}