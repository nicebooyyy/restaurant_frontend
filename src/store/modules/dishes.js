import axios from 'axios';
import cake from "../../assets/cake.jpg";
import fries from "../../assets/fries.jpg";
import macroni from "../../assets/macroni.jpg";
import noodles from "../../assets/noodles.jpg";
import pizza from "../../assets/pizza.jpg";
import salad from "../../assets/salad.jpg";
import samosa from "../../assets/samosa.jpg";
import soup from "../../assets/soup.jpg";
import tacos from "../../assets/tacos.jpg";

export default {
    //визачаємо початкові дані(початковий стан)
    state: {
        topRated: [
            {
                _id : 1,
                name : "Cake",
                price : 100,
                url : cake,
                desc : "Very Taste Cake",
                rate: 4.1
            },
            {
                _id : 2,
                name : "Fries",
                price : 100,
                url : fries,
                desc : "Very Taste Fries",
                rate: 4.1
            },{
                _id : 3,
                name : "Macroni",
                price : 100,
                url : macroni,
                desc : "Very Taste Macroni",
                rate: 4.1
            },
        ],
        allCategories: [
            {
                _id : 4,
                name : "Noodles",
                price : 100,
                url : noodles,
                desc : "Very Taste Noodles",
                rate: 4.1
            },
            {
                _id : 5,
                name : "Pizza",
                price : 100,
                url : pizza,
                desc : "Very Taste Pizza",
                rate: 4.1
            },{
                _id : 6,
                name : "Salad",
                price : 100,
                url : salad,
                desc : "Very Taste Salad",
                rate: 4.1
            },
        ],
        dishesNearYou: [
            {
                _id : 7,
                name : "Samosa",
                price : 100,
                url : samosa,
                desc : "Very Taste Samosa",
                rate: 4.1
            },
            {
                _id : 8,
                name : "Soup",
                price : 100,
                url : soup,
                desc : "Very Taste Soup",
                rate: 4.1
            },{
                _id : 9,
                name : "Tacos",
                price : 100,
                url : tacos,
                desc : "Very Taste Tacos",
                rate: 4.1
            },
        ],
        cartItemCount: 0,
        cartItems: [],
        dishes: [],
    },
    //функції які змінюють store(сховище)
    mutations: {
        //заношу список страв в сховище
        updateStateDishes (state,dishes) {
            state.dishes = dishes;
        },
        addToCart (state,payload) {
            
            let item = payload;
            console.log(item);
            item = {...item, quantity: 1};
            if (state.cartItems.length >= 1) {
               let bool = state.cartItems.some(i=>i._id === item._id);
               if (bool) {
                   let itemIndex = state.cartItems.findIndex(el=>el._id === item._id);
                   state.cartItems[itemIndex]["quantity"] += 1;
               } else {
                   state.cartItems.push(item);
               }
            } else {
                state.cartItems.push(item);
            }
            state.cartItemCount++;
        },
        removeItem (state,payload) {
            if (state.cartItems.length > 0) {
                let bool = state.cartItems.some(i=>i._id === payload._id)
                if (bool) {
                    let index = state.cartItems.findIndex(el=>el._id === payload._id)
                   if (state.cartItems[index]["quantity"] !== 0) {
                        state.cartItems[index]["quantity"] -= 1
                        state.cartItemCount--
                   }
                   if (state.cartItems[index]["quantity"] === 0) {
                    state.cartItems.splice(index,1)
                    }
                    
                }
            }
        },
        clearCart (state) {
            state.cartItems = [];
            state.cartItemCount= 0;
            
		},
    },
    
	
    //для роботи з бекендом
    actions: {
        //отримую список страв з сервера
        async serverDishes (context) {
            const response  = await axios.get('http://localhost:3000/dishes');
            const dishes =  response.data.dishes;
            context.commit('updateStateDishes',dishes);
        },
       addToCart: (context,payload) => {
           context.commit("addToCart", payload);
       },
       removeItem: (context,payload) => {
        context.commit("removeItem", payload);
        },
        clearCart: (context) => {
            context.commit('clearCart');
        }
    },
    //трансформація даних та отримання їх зі сховища
    getters: {
        allDishes (state) {
            console.log(state.dishes);
            return state.dishes;
        },
        topRated (state) {
            return state.topRated;
        },
        allCategories (state) {
            return state.allCategories;
        },
        dishesNearYou (state) {
            return state.dishesNearYou;
        },
        cartItemCount (state) {
            return state.cartItemCount;
        },
    },
}