import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { ValidationObserver } from 'vee-validate';
import { ValidationProvider } from 'vee-validate';
import vuetify from '@/plugins/vuetify' // path to vuetify export
import axios from 'axios';
import CKEditor from '@ckeditor/ckeditor5-vue';




axios.interceptors.response.use(
  response => {
    if (response.status === 200 || response.status === 201) {
      return Promise.resolve(response);
    } else {
      return Promise.reject(response);
    }
  },
error => {
    if (error.response.status) {
      switch (error.response.status) {
        // case 400:
         
        //  //do something
        //   break;
      
        // case 401:
        //   alert("session expired");
        //   break;
        case 403:
          router.replace({
            path: "/login",
            // query: { redirect: router.currentRoute.fullPath }
          });
           break;
        case 404:
          alert('page not exist');
          break;
        case 502:
         setTimeout(() => {
            router.replace({
              path: "/login",
              query: {
                redirect: router.currentRoute.fullPath
              }
            });
          }, 1000);
      }
      return Promise.reject(error.response);
    }
  }
);

// Register it globally
// main.js or any entry file.
Vue.component('ValidationObserver', ValidationObserver);
Vue.component('ValidationProvider', ValidationProvider);

Vue.config.productionTip = false
Vue.use(vuetify);

Vue.use( CKEditor );
new Vue({
  router,
  store,
  // created(){
    
  // },
  vuetify,
  render: h => h(App)
}).$mount('#app')

