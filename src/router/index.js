import Vue from 'vue'
import VueRouter from 'vue-router'

import Login from '../views/Auth/Login.vue';
import Register from '../views/Auth/Register.vue';

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import('@/components/guest/Home')
  },
  {
    path: '/home',
    component: () => import('@/components/guest/Home'),
    beforeEnter: requireAuth,
  },
  {
    path: '/login',
    component: Login
  },
  {
    path: '/register',
    component: Register
  },
  {
    path: '/profile',
    name: 'profile',
    // lazy-loaded
    component: () => import('../views/Boards/Profile.vue')
  },
  // {
  //   path: '/mod',
  //   name: 'moderator',
  //   // lazy-loaded
  //   component: () => import('../views/Boards/BoardModerator.vue')
  // },
  {
    path: '/user',
    name: 'user',
    // lazy-loaded
    component: () => import('../views/Boards/BoardUser.vue')
  },
  { 
    path: '/404', component: () => import('../views/Errors/NotFound.vue') 
  },  
  { 
    path: '*', redirect: '/404' 
  },
  {
    path: '/cart',
    name: 'Cart',
    component: () => import('@/components/guest/Cart')
  },  
  {
    path: '/done',
    name: 'OrderDone',
    component: () => import('@/components/guest/OrderDone')
  }, 
  
  {
    path: '/admin',
    components: {
      default:() => import('@/components/admin/App'),
      helperAdmin:() => import('@/components/admin/Helper')
    },
    children: [
      {
        path: '',
        component: () => import('@/components/admin/Home'),
      },
      {
        path: 'dish',
        component: () => import('@/components/admin/dish/Dish'),
      },
      {
        path: 'order',
        component: () => import('@/components/admin/order/Order'),
      },
      {
        path: 'user',
        component: () => import('@/components/admin/user/User'),
      },
      // {
      //   path: 'forecast',
      //   component: () => import('@/components/admin/forecast/Forecast'),
      // },
      {
        path: 'forecast',
        name: 'Forecast',
        component: () => import( '@/components/admin/forecast/RandomChart.vue')
        // component: () => import('@/components/admin/forecast/Holt.vue')
      },
      {
        path: 'category',
        component:() => import('@/components/admin/category/Category'),
      }
    ],
    
  }, 
  {
    path: '/cook',
    components: {
      default:() => import('@/components/cook/App'),
      helperAdmin:() => import('@/components/cook/Helper')
    },
    children: [
      {
        path: '',
        component: () => import('@/components/cook/Home'),
      },
      {
        path: 'order',
        component: () => import('@/components/cook/order/Order'),
      },
    ],
    
  }, 
  {
    path: '/waiter',
    components: {
      default:() => import('@/components/waiter/App'),
      helperAdmin:() => import('@/components/waiter/Helper')
    },
    children: [
      {
        path: '',
        component: () => import('@/components/waiter/Home'),
      },
      {
        path: 'order',
        component: () => import('@/components/waiter/order/Order'),
      },
    ],
    
  }, 
  
  

]
export function requireAuth(to, from, next) {
  const publicPages = ['/login', '/register', '/home'];
  const authRequired = !publicPages.includes(to.path);
  const loggedIn = localStorage.getItem('user');

  // trying to access a restricted page + not logged in
  // redirect to login page
  if (authRequired && !loggedIn) {
    next('/login');
  } else {
    next();
  }
}
const router = new VueRouter({
  routes
})


export default router
